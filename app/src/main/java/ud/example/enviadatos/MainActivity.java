package ud.example.enviadatos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ud.example.enviadatos.Clases.Bebida;
import ud.example.enviadatos.Clases.Hamburguesa;

public class MainActivity extends AppCompatActivity {

    private EditText cliente;
    private Spinner lista01; // tipo hamburguesa
    private Spinner lista02; //con cebolla
    private Spinner lista03; // tipo bebida
    private Spinner listaCantHamb;
    private Spinner listaCantBeb;
    private CheckBox hielo; // con hielo
    private Button enviab; // botón

    private List<String> tipoh;
    private List<String> sino;

    public void datosinicio() {
        List<String> tipoh = new ArrayList<String>();
        List<String> tipob = new ArrayList<String>();
        List<String> sino = new ArrayList<String>();
        List<String> cantd = new ArrayList<String>();
        cantd.add("1");
        cantd.add("2");
        cantd.add("3");
        cantd.add("4");
        cantd.add("5");

        tipoh.add("Carne");
        tipoh.add("Pollo");
        tipoh.add("Pescado");
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>
                (this,android.R.layout.simple_spinner_item, tipoh);
        adaptador.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        lista01.setAdapter(adaptador);

        sino.add("SI");
        sino.add("NO");
        ArrayAdapter<String> adaptador02 = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, sino);
        adaptador02.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        lista02.setAdapter(adaptador02);
        listaCantHamb.setAdapter(new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, cantd));
        listaCantBeb.setAdapter(new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, cantd));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cliente = (EditText) findViewById(R.id.editText);
        lista01 = (Spinner) findViewById(R.id.spinner);
        lista02 = (Spinner) findViewById(R.id.spinner2);
        lista03 = (Spinner) findViewById(R.id.spinner3);
        listaCantHamb = findViewById(R.id.spCantHamb);
        listaCantBeb = findViewById(R.id.spCantBeb);
                hielo = (CheckBox) findViewById(R.id.checkBox);
        enviab = (Button) findViewById(R.id.button);
        datosinicio();
    }

    public void llamaractividad(View v) {
        String nombre = cliente.getText().toString();
        if (nombre.isEmpty()){
            Toast.makeText(this, "Por favor digite el nombre del cliente", Toast.LENGTH_LONG).show();
            return;
        }
        boolean cebolla = false;
        if(lista02.getSelectedItem().toString().equals("SI")) cebolla = true;
        Hamburguesa miham = new
                Hamburguesa(lista01.getSelectedItem().toString(), cebolla);
        boolean hielo = this.hielo.isChecked();
        Bebida mibeb = new Bebida(lista03.getSelectedItem().toString(),hielo);

        // Creamos el Intent
        Intent int01 = new Intent(this, Actividad2.class);

        // Primera forma de pasar parametros
        int01.putExtra("CLIENTE", nombre);
        // Segunda forma de pasar parametros
        int01.putExtra("HAMBURGUESA", miham);
        // Tercera forma de pasar parametros
        int01.putExtra("BEBIDA", mibeb);

        startActivity(int01);
    }
}