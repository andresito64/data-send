package ud.example.enviadatos;

import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ud.example.enviadatos.Clases.Bebida;
import ud.example.enviadatos.Clases.Hamburguesa;

public class Actividad2 extends AppCompatActivity {

    private TextView logTextView;
    private ScrollView scrollView;

    private void log(String s) {
        logTextView.append("\n" + s);

        // Movemos el Scroll abajo del todo
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    } // end log

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad2);

        logTextView = (TextView) findViewById(R.id.textView10);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        // Recuperamos la información pasada en el intent

        Bundle bundle = this.getIntent().getExtras();
        log("Cliente: " + bundle.getString("CLIENTE"));
        log("");
        Hamburguesa mipedham = (Hamburguesa) bundle.get("HAMBURGUESA");
        Bebida mipedbeb = (Bebida) bundle.get("BEBIDA");
        log("Hamburguesa");
        log(mipedham.getTipo());
        if (mipedham.getCebolla()) {log("con cebolla");
        }else{log("sin cebolla");}
        log("");
        log("Bebida");
        log(mipedbeb.getTipo());
        if (mipedbeb.getHielo()){log("con hielo");
        } else {log("sin hielo");}
    }
}
